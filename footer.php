<footer class="page-footer cyan darken-2 white-text">

	<div class="container">
		
		<div class="row">
			
			<div class="col l6 s12">
				<h5 class="">Centro Médico del Centro</h5>
			</div>

		</div>

	</div>
	<div class="cyan darken-3 white-text footer-copyright">
		
		<div class="container">
			© <i>2018</i> Derechos Reservados
			<a class="white-text right">Unepic</a>
		</div>

	</div>

</footer>