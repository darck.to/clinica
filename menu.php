<div class="navbar-fixed">
	
	<nav class="cyan darken-3 z-depth-0">
			
		<div class="container nav-wrapper">
			
			<div class="row cyan darken-3 no-m"><div class="ten-padding-top"></div></div>

			<a href="index.php" class="brand-logo">
				<img class="responsive-img" src="img/logo_header.png">
			</a>

			<a href="index.php" data-target="mobile-nav" class="sidenav-trigger"><i class="material-icons">menu</i></a>

			<ul id="nav-mobile" class="right hide-on-med-and-down">
				
				<li><a href="index.php">Inicio</a></li>
				<li><a href="servicios.php">Servicios</a></li>
				<li><a class="openContact handed">Contacto</a></li>
				<div id="contact-div" class="contact-container col s12 m3"></div>
				<!-- Show Results -->
				<div id="navres-container" class="row hide">
					<ul class="col s12 m12 l12 z-depth-1 white" id="navres"></ul>
				</div>

			</ul>


		</div>

		<div class="row cyan darken-3 no-m"><div class="ten-padding-top"></div></div>
	
	</nav>

</div>

<ul class="sidenav cyan darken-3" id="mobile-nav">
	
	<li class="logo fiften-padding"><a id="logo-container" href="." class="brand-logo"><img class="responsive-img" src="img/logo_header.png"></a></li>
	<li><a href="index.php">Inicio</a></li>
	<li><a class="openContact handed" class="sidenav-close">Contacto</a></li>
	<li><a href="servicios.php">Servicios</a></li>

</ul>

<?php include('init.php');?>

<script type="text/javascript">

$(document).ready(function(){
	
	$('.openContact').on('click',function(){
		$(this).addClass('white');
		$(this).addClass('black-text');
		var appear = $("#contact-div").html();
		if (appear) {
			$("#contact-div").html('');
			$(this).removeClass('white');
			$(this).removeClass('black-text');
		} else {
			$("#contact-div").load('templates/contact.html');
		}
	});
	
});
	

</script>